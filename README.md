# 3D Models

This repository contains various 3D models suitable for printing or remixing.
Where possible, each model is provided in multiple formats:

- Fusion 360 `f3d` file
- 3D Model `step` file
- Mesh `stl` File
- Image `jpg` File

Unless otherwise noted, all model and files are released under the MIT open source license (see `LICENSE` for details).

**Models:**

[[_TOC_]]


## Down Camera Stand

<table border="0"><tr><td width="48%">
The down-camera stand is designed for making it easy to use a smartphone as a camera during online streaming or for product shots
where the camera is directly over the subject material. It is best suited for smaller objects.

The model is designed using the Fusion 360 parametric method for the overall dimensions.
This simplifies changing the size.

The model is made up of a top, bottom, and 2 identical sides.
The parts snap fit together.
</td><td width="48%" align="center">
<img src="down_camera/down_camera_stand.jpg" alt="Down Camera Stand"></a>
<br/>
<i>Down Facing Camera Stand for Smartphone</i>
</td></tr></table>



## Rotary Table

<table border="0"><tr><td width="48%">
The rotary table has been designed as part of three DIY tools for reducing the effort to hand assemble PCBs.

SMT components, used for assembling PCBs, frequently are supplied on tapes.
These tapes may be full reels or may be cut tapes for smaller quantities.
The components of a tape have a uniform orientation.

PCB assembly equipment takes components from tapes and then rotates each one to place them in the correct location and orientation on the PCB.
Rotating components by hand is very tedious and error prone.
A better solution is to rotate the PCBs to match the orientation from the component tapes.

The rotary table hods up to 6 PCBs at a time and through gear teeth, provide a quick and convenient method of rotating all of the PCBs the same amount.

</td><td width="489" align="center">
<img src="rotary_table/rotary_table.jpg" alt="Rotary Table"></a>
<br/>
<i>rotating table with rotating platforms - used for hand assembling PCBs</i>
<br/><br/>
</td></tr></table>


### Fabrication

The rotary table may be 3D printed or laser cut from 3mm (or 5mm) acrylic.
The design files assume an approximately 4mm axis for each gear.
This happens to be the outside diameter of a 3D printer's bowden tube PTFE material.
The base has holes which are slightly undersized and the gears have hole which are slightly oversized.

The 3D printed base has been rendered as 1/3rd of a circle to fit on most 3D printers.
Print three identical pieces and glue them together.

A single 60mm gear is needed for the center.

Six 50mm gears are needed for the "platforms" where PCBs will be placed.

Some type of rubbery material should be added to the platform gears.
Two options have been tested: spray-on "tool dip" coating, and rubber mesh shelf protector.

<table border="0"><tr>
<td width="32%" align="center">
  <img src="rotary_table/base.jpg" alt="Table Base"></a>
  <br/>
  <i>the base may be printed in three sections</i>
  <br/><br/>
<td width="32%" align="center">
  <img src="rotary_table/rubber_pads.jpg" alt="Rubber Shelf Covering"></a>
  <br/>
  <i>rubber mat or spray material prevents PCBs from slipping</i>
  <br/><br/>
</td>
<td width="32%" align="center">
  <img src="rotary_table/center_pin.jpg" alt="Center Push Pin"></a>
  <br/>
  <i>there is a hole at the center for a push pin</i>
  <br/><br/>
</td>
</tr></table>


### Usage

The usage of the rotary table is intuitive.
That said, there is one recommendation to improve its usage.
It is handy to use a push pin in the center.
The whole table will rotate around the pin.
This makes it easy to sequence through the PCBs on the outer platforms.
Then, use the center gear to rotate all of the PCBs to the orientation of the component(s) about to be placed.

### Tips

The platform gears are 100mm in diameter.
The 3D printed versions are a little taller than the center gear.
The rubber shelf protector material makes them a bit taller still.
This characteristic has an advantage.
When working with PCBs which are more than 100mm in a dimension, just remove alternating platform gears.

_For really big PCBs, it is possible to add material to the center gear so it is taller than the outer gears. Then remove all but one outer gear. Now place the PCB in the center and use the remaining outer gear to rotate the center._


## SMT Component Dispensers

<table border="0"><tr><td width="48%">
SMT components, used for assembling PCBs, frequently are supplied on tapes.
These tapes may be full reels or may be cut tapes for smaller quantities.
The components of a tape have a uniform orientation.

The most common width of these tapes are: 8mm, 12mm, and 16mm

The depth of the pockets within the tape will vary with the components.
Most passives - i.e. resistors, capacitors, etc - will be very thin (less than 1mm) while buttons and connectors may be quite thick (4mm or more).

The `stl` files provide the most common defaults.
Custom dispensers may be created using the Fusion 360 `f3d` file.
Alternately, use STEP file as a reference for creating dispensers with other CAD tools.

</td><td width="48%" align="center">
<img src="smt_dispenser/smt_dispenser.jpg" alt="SMT Dispensers"></a>
<br/>
<i>SMT dispensers and end cover</i>
<br/><br/>
</td></tr></table>

### Fabrication

The 3D printable STL files are for the most common tape sizes and pocket depths.

The Fusion 360 `f3d` file uses parametric values for the critical dimensions: `tape_depth` and `tape_width`.
If you need custom dispensers, it may be possible to alter the `STEP` files. Otherwise, either use Fusion 360 or roll-your-own dispensers.

Only one cover is needed at the end of a bank of dispensers.
The dispensers are stacked side-by-side and held together with 4-20 or 3mm threaded rods.
Only 2 rods are needed _(even though four holes are provided)_.


<table border="0"><tr>
<td width="32%" align="center">
  <img src="smt_dispenser/sizes.jpg" alt="Default Sizes"></a>
  <br/>
  <i>examples of the 3D printed files</i>
  <br/><br/>
<td width="32%" align="center">
  <img src="smt_dispenser/loaded.jpg" alt="Tapes in Dispensers"></a>
  <br/>
  <i>dispensers with cut tapes installed</i>
  <br/><br/>
</td>
<td width="32%" align="center">
  <img src="smt_dispenser/dispenser_stack.jpg" alt="Sample Stack"></a>
  <br/>
  <i>example stack of dispensers</i>
  <br/><br/>
</td>
</tr></table>


## Vacuum Pen, Human Powered

<table border="0"><tr><td width="48%">
The human powered vacuum pen is used for 'picking' SMT components from parts tapes (such as those in the 3D printed SMT Dispensers).

There are no design files. The fabrication is very dependent on what the maker has available. _(see **Fabrication** below)_

Consider the human powered vacuum pen as starting point. If it gets lots of use then a commercial product is a better option.
There are several available. The [Pixel Pump](https://shop.robins-tools.com) is a popular solution.

</td><td width="48%" align="center">
<img src="vacuum_pen/vacuum_peb.jpg" alt="Vacuum Pen"></a>
<br/>
<i>the Human Powered Vacuum Pen</i>
<br/><br/>
</td></tr></table>

### Fabrication

The human powered vacuum pen has the following parts:
1. one 10ml syringe [affiliate link](https://a.co/d/9Z64Wqu)
1. blunt tip needle (in various sizes) \* [affiliate link](https://a.co/d/cAkpJZe)
1. two small zip ties [affiliate link](https://a.co/d/iSQBqJN)
1. 3 to 5 foot length of tubing (1.5m) - oxygen tubing worked well [affiliate link](https://a.co/d/9DMRSGP)
1. hot glue

\* The most useful needle gauge is 20ga followed by 18ga and 22ga.
Larger gauge (smaller numbers) work for microcontroller chips and connectors.
The needle must be curved. Curved needles are available is assortment packs.

**Note:** Most of the parts are easily available but not in single or small quantity.
For this reason, it is useful to coordinate with other makers or a MakerSpace to fabricate several vacuum pens at the same time.

Steps:
1. Remove the plunger from the syringe
1. Remove the black rubber end from teh plunger
1. Cut an 'X' slit in the end of the rubber such that it is a tight fit for the tubing
1. _If the tubing has a hard plastic fitting on the end, there is no need to cut it off_
1. Insert one end of the tubing through the "X" in the rubber plunger end
1. Use one zip tie to trap the little flaps of the "X" around the tubing. Trim the zip tie.
1. Apply a small amount of hot glue over the zip tie, avoiding any glue from covering the end of the tubing
1. While teh glue is still hot, insert the rubber plunger back into the syringe approximately 3/4 of an inch (20mm). Hold the syringe pointing upward so the hot glue stays close to teh rubber plunger
1. Once the hot glue has cooled, tip the syringe pointing down and fill the areas from teh back of teh black plunger to teh rim of the syringe with hot glue. Hold the syringe adn the tubing to keep the tubing centered until the hot glue cools.
1. Optional: Add a zip tie to the opposite end of the tubing and trim. Apply hot glue over teh zip tie. Slowly rotate teh tubing to keep hte hot glue is a ball shape as it cools.

_photos will be added shortly_
<!--
<table border="0"><tr>
<td width="32%" align="center">
  <img src="vacuum_pen/rubber plunger.jpg" alt="rubber plunger with zip tie"></a>
  <br/>
  <i>Rubber Plunger Zip-tied to Tubing</i>
  <br/><br/>
<td width="32%" align="center">
  <img src="vacuum_pen/glue_plunger.jpg" alt="hot glue plunger"></a>
  <br/>
  <i>Plunger Hot Glued in Syringe</i>
  <br/><br/>
</td>
<td width="32%" align="center">
  <img src="vacuum_pen/glue_ball.jpg" alt="hot glue ball"></a>
  <br/>
  <i>Hot Glue Ball on Tubing</i>
  <br/><br/>
</td>
</tr></table>
 -->

<!---
## Dummy Entry

<table border="0"><tr><td width="48%">
model details
</td><td width="48%" align="center">
<img src="model/model.jpg" alt="model name"></a>
<br/>
<i>model short description</i>
<br/><br/>
</td></tr></table>
-->
